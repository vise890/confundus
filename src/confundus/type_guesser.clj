(ns confundus.type-guesser
  "A bunch of utilities to try and parse strings into more meaningful data
  types."
  (:require [clojure.string :as str]))

(defn parse-boolean
  [s]
  (let [bool-s (some-> s
                  str/lower-case
                  #{"true" "false"})]
    (if-not bool-s
      (throw (IllegalArgumentException. (str "not a boolean:" s)))
      (Boolean/parseBoolean bool-s))))

(defn parse
  ([s]
   (parse [#(java.time.LocalDate/parse %)
           #(java.time.LocalTime/parse %)
           #(java.time.LocalDateTime/parse %)
           #(java.time.Instant/parse %)
           parse-boolean
           #(Integer/parseInt %)
           #(Long/parseLong %)
           #(java.math.BigInteger. %)
           #(java.math.BigDecimal. %)
           str]
          (str/trim s)))
  ([ps s]
   (let [[p & ps] ps]
     (try (p s)
          (catch Exception _
            (parse ps s))))))
