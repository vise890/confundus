(ns confundus.obfuscator
  "Util for replacing values with randomly generated ones. Tries to preserve
  type."
  (:require [clojure.spec.alpha :as s]
            [clojure.spec.gen.alpha :as sgen]))

(def ^:private preds
  [string?

   bytes?

   pos-int?
   nat-int?
   neg-int?

   int?
   integer?
   float?
   decimal?

   boolean?
   nil?

   simple-keyword?
   qualified-keyword?

   symbol?

   inst?])

(defn obfuscate [x]
  (loop [[p & ps] preds]
    (if-not p
      ;; generate a good old string if no predicate matches
      (sgen/generate (s/gen string?))
      (if (p x)
        (sgen/generate (s/gen p))
        (recur ps)))))
