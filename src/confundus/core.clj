(ns confundus.core
  (:gen-class)
  (:require [clojure.data.csv :as csv]
            [clojure.java.io :as io]
            [confundus.obfuscator :as o]
            [confundus.type-guesser :as tg]))

(defn obfuscate-cells
  [cells]
  (map (comp
        (partial map o/obfuscate)
        (partial map tg/parse))
       cells))

(defn -main
  [& args]
  (let [filename-in (first args)
        filename-out
        (str filename-in ".obfuscated.csv")]
    (with-open [in (io/reader filename-in)
                out (io/writer filename-out)]
      (->> (csv/read-csv in)
           obfuscate-cells
           (csv/write-csv out)))))

(comment
  ;;
  (-main "./test/resources/fixture.csv")
  ;;
)
