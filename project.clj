(defproject confundus "0.1.0-SNAPSHOT"
  :description "Small utility to obfuscate CSV and JSON files."
  :url "https://gitlab.com/vise890/confundus"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[me.raynes/fs "1.4.6"]
                 [org.clojure/clojure "1.9.0"]
                 [org.clojure/data.csv "0.1.4"]
                 [org.clojure/spec.alpha "0.2.168"]
                 [org.clojure/test.check "0.10.0-alpha3"]]
  :main ^:skip-aot confundus.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
