(ns confundus.type-guesser-test
  (:require [clojure.test :refer :all]
            [confundus.type-guesser :as sut]))

(deftest parse-test
  (testing "parses integers"
    (is (= 10402 (sut/parse "10402")))
    (is (= 42 (sut/parse "42")))
    (is (= -1 (sut/parse "-1")))
    (is (= -1 (sut/parse " \t  -1 "))) ; with space!
    (is (= 1 (sut/parse "01"))))

  (testing "parses decimals"
    (is (= 0.1M (sut/parse "0.1")))
    (is (= -0.41M (sut/parse "-0.41"))))

  (testing "parses booleans"
    (is (= true (sut/parse "true")))
    (is (= false (sut/parse "false")))
    (is (= false (sut/parse "fAlSe"))))

  (testing "parses local dates"
    (is (= (java.time.LocalDate/parse "2011-12-03")
           (sut/parse "2011-12-03"))))

  (testing "parses local times"
    (is (= (java.time.LocalTime/parse "10:15:30")
           (sut/parse "10:15:30"))))

  (testing "parses local date times"
    (is (= (java.time.LocalDateTime/parse "2011-12-03T10:15:30")
           (sut/parse "2011-12-03T10:15:30"))))

  (testing "parses instants"
    (is (= (java.time.Instant/parse "2011-12-03T10:15:30Z")
           (sut/parse "2011-12-03T10:15:30Z"))))

  (testing "returns the string unmodified if it cannot parse it into anything more meaningful"
    (is (= "x0.1" (sut/parse "x0.1")))))
