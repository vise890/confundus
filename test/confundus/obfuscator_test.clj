(ns confundus.obfuscator-test
  (:require [clojure.test :refer :all]
            [confundus.obfuscator :as sut]))

(defmacro is-obfuscated
  "checks that x is obfuscated and that the result matches p"
  [p x]
  `(let [got# (sut/obfuscate ~x)]
     (is (not (= ~x got#))
         (str "obfuscated result should be different, got the same: " got#))
     (is (~p got#)
         (str got# " should satisfy predicate " ~p))
     got#))

(deftest obfuscate-test
  (is-obfuscated string? "foo")

  (is-obfuscated bytes? (byte-array [1 2]))

  (is-obfuscated int? 1)
  (is-obfuscated pos-int? 1)
  (is-obfuscated neg-int? -1)
  (is-obfuscated nat-int? 0)
  (is-obfuscated integer? 42N)
  (is-obfuscated float? 0.1)
  (is-obfuscated decimal? 1M)

  (is (boolean? (sut/obfuscate true)))
  (is (nil? (sut/obfuscate nil)))

  (is-obfuscated simple-keyword? :foo)
  (is-obfuscated qualified-keyword? :foo/bar)

  (is-obfuscated symbol? 'foo)

  (is-obfuscated inst? (java.time.Instant/now))

  (testing "defaults to string if it can't infer type"
    (is (string? (sut/obfuscate (atom :foo))))))
