(ns confundus.core-test
  (:require [clojure.test :refer :all]
            [confundus.core :as sut]
            [me.raynes.fs :as fs]))

(deftest a-test
  (let [in-f  "./test/resources/fixture.csv"
        out-f "./test/resources/fixture.csv.obfuscated.csv"]
    (fs/delete out-f)
    (sut/-main in-f)
    ; wow. such test
    (is (fs/file? out-f))))
