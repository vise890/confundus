# confundus

Small utility to obfuscate CSV and JSON files.

## What it does

Turns this:
```csv
beep,boop
123,456
```

.. into this:
```csv
E2vH6c7x36r7aS6Fgi2oan,7Mt7QB3b1Hib7fGDX28Ad42WfX
512,-2248
```

## Usage

```
lein run ./test/resources/fixture.csv
```
## License

Copyright © 2018 Martino Visintin

Distributed under the Eclipse Public License either version 1.0 or (at your
option) any later version.
